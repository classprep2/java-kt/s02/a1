package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);


        while(true){
            System.out.println("Input year to be checked if leap year: ");
            int year = myObj.nextInt();

            if(year%4==0){
                System.out.println(year + " is a leap year");

            }
            else{
                System.out.println(year + " is NOT a leap year");
            }
            System.out.println("------------");
        }

    }
}
