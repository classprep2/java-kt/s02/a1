package com.zuitt.example;
import java.util.ArrayList; // import the ArrayList class
import java.util.HashMap; // import the HashMap class

public class S2A2 {
    public static void main(String[] args){
        int[] prime = new int[5];
        prime[0] = 2;
        prime[1] = 3;
        prime[2] = 5;
        prime[3] = 7;
        prime[4] = 11;

        System.out.println("The first prime number is "+prime[0] + ".");
        System.out.println("The second prime number is "+prime[1] + ".");
        System.out.println("The third prime number is "+prime[2] + ".");
        System.out.println("The fourth prime number is "+prime[3] + ".");
        System.out.println("The fifth prime number is "+prime[4] + ".");

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are " +friends +".");


        HashMap<String, Integer> items = new HashMap<String, Integer>();
        items.put("toothpaste", 15);
        items.put("toothbrush", 20);
        items.put("soap", 12);
        System.out.println("Our current inventory consists of: "+items+".");

    }
}
